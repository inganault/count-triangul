with open('log.csv') as inp:
	with open('log_prune.csv', 'wt', newline='') as out:
		for line in inp:
			if line.startswith('#') or line.startswith('l'):
				out.write(line)
				continue
			last_node, *_ = line.split(',',2)
			last_node = int(last_node)
			if (last_node + 1) % 100 == 0:
				out.write(line)
				continue
			if last_node < 100 and (last_node + 1) % 25 == 0:
				out.write(line)
				continue
