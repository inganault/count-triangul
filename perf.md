# Time used

| Job                 | Worker*Thread | Chunking       | RAM per worker      | Time total(s) | Time * Parallelism |
| ------------------- | ------------- | -------------- | ------------------- | ------------: | -----------------: |
| triangul.rs         |     1 * 16    | 1 node/chunk   | ~3 GB (2GB no path2)|         34.48 |             551.68 |
| triangul.rs         |     1 * 4     | 1 node/chunk   | ~3 GB (2GB no path2)|         47.72 |             190.86 |
| repjoin2.py HASH=F  |     6 * 1     | 1000 chunks    | ~3 GB               |        102.00 |             612.00 |
| repjoin2.py HASH=F  |     4 * 1     | 1000 chunks    | ~3 GB               |        112.94 |             451.76 |
| repjoin2.py HASH=T  |     4 * 1     | 1000 chunks    | ~7 GB               |        120.75 |             483.00 |
| repjoin1.py EDGES=T |     5 * 1     | 1000 chunks    | ~5 GB               |       1036.95 |            5184.75 |
| repjoin1.py EDGES=T |     5 * 1     | 4000 chunks    | ~5 GB               |       1949.72 |           23396.64 |
| repjoin1.py EDGES=T |     5 * 1     |  100 chunks    | ~5 GB               |       2534.69 |           12673.45 |
| repjoin1.py EDGES=F |    12 * 1     |  100 chunks    | ~0.3 GB             |       2748.78 |           32985.36 |
| repjoin1.py EDGES=F |    12 * 1     | 1000 chunks    | ~0.05 GB            |       6251.27 |           75015.24 |
| repjoin1.py EDGES=F |    12 * 1     |   23 chunks    | ~0.8 GB             |       7643.74 |           91724.88 |

- `repjoin2.py HASH=F` is same algorithm as `triangul.rs`

## Environment

Python: PyPy 3.10  
Rust: release mode opt-level=3  

CPU: AMD Ryzen 9 5900HS (8 Core 16 SMT)  
RAM: 32GB DDR4 3200MHz  
OS: Windows 10  
