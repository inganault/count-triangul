
from pathlib import Path
import time
import multiprocessing
import sys

# Run with PyPy for better performance

INPUT_PATH = Path(sys.argv[1]) if len(sys.argv) > 1 else Path('testcase')
EDGES = INPUT_PATH / 'edges.csv'
NODES = INPUT_PATH / 'nodes.csv'

PARSE_EDGES = True
SHOW_PROGRESS = True
WORKER_COUNT = 5 if PARSE_EDGES else 12
CHUNK_COUNT = 1000

def setup(master=False, log=False):
    # Setup, 1 time per worker
    global edges_iter, node_max
    if master or not PARSE_EDGES:
        # Get node_max from nodes.csv
        with open(NODES) as fp:
            last_line = None
            for line in fp:
                if len(line) > 1:
                    last_line = line
            node_max = int(last_line)
        # edges is lazy iterator
        edges_iter = edges_gen
    else:
        # Parse edge to array in memory
        edges = []
        node_max = 0
        if log:
            print(f'Reading edges')
        start = time.monotonic()
        with open(EDGES) as fp:
            for lin in fp:
                fro,to = map(int,lin.split(','))
                if fro == to:
                    continue
                edges.append((fro,to))
                node_max = max(node_max, fro, to)
        if log:
            print(f'Reading edges [{time.monotonic()-start:.2f} sec]')
        edges_iter = lambda: edges

def edges_gen():
    with open(EDGES) as fp:
        for lin in fp:
            fro,to = map(int,lin.split(','))
            if fro == to:
                continue
            yield (fro,to)

from collections import defaultdict
def mapper(set_n):
    count = 0
    edge_from_lower = defaultdict(set)
    edge_from_upper = defaultdict(set)
    to_filter = set()
    for fro,to in edges_iter():
        if fro not in set_n and to not in set_n:
            continue
        if to in set_n:
            if fro < to:
                edge_from_lower[to].add(fro)
                to_filter.add(fro)
        if fro in set_n:
            if to < fro:
                edge_from_upper[to].add(fro)

    for fro,to in edges_iter():
        if fro not in edge_from_upper or to not in to_filter:
            continue
        for upper in edge_from_upper[fro]:
            if to in edge_from_lower[upper]:
                count += 1
                # print(f'TRI  {fro} -> {to} -> {upper}')
    if SHOW_PROGRESS:
        progress = ((max(set_n)-1) / node_max) * 100
        print(f'Progress: {progress:5.2f}%', end='\r')
    return count

if __name__ == '__main__':
    setup(master=True)
    chunk_size = max(1, (node_max+CHUNK_COUNT-1)//CHUNK_COUNT)
    chunks = (set(range(node, node+chunk_size)) for node in range(1, node_max+1, chunk_size))
    start = time.monotonic()
    if SHOW_PROGRESS:
        print('Processing')
    with multiprocessing.Pool(WORKER_COUNT) as p:
        count = sum(p.imap(mapper, chunks))
    if SHOW_PROGRESS:
        print(f'Finished [{time.monotonic()-start:.2f} sec]')
        print('\n------')
    print(f'Count = {count}')
else:
    setup(master=False)
