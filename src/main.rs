use argh::FromArgs;
use mimalloc::MiMalloc;
use rayon::prelude::*;
use std::{
    cmp::max,
    io::{BufRead, Write},
    path::PathBuf,
    time::Instant,
};

#[global_allocator]
static GLOBAL: MiMalloc = MiMalloc;

#[derive(Debug, Default)]
struct Context {
    // dataset
    edge_to_lower: Vec<Vec<u32>>,
    edge_from_lower: Vec<Vec<u32>>,
    edge_to_upper: Vec<Vec<u32>>,   // for path2
    edge_from_upper: Vec<Vec<u32>>, // for path2

    // stats
    node_max: u32,
    edge_count: u32,

    dump: bool,
}

#[derive(Debug, Default, Clone, PartialEq, Eq)]
struct Stat {
    last_node: u32,
    edge_processed: usize,
    path2_count: usize,
    triangle_found: usize,
}

impl Context {
    fn new() -> Self {
        Self {
            node_max: 0,
            ..Default::default()
        }
    }

    fn parse_edge(&mut self, fp: std::fs::File) {
        let (send, recv) = std::sync::mpsc::sync_channel(128);
        {
            std::thread::spawn(move || {
                let buf_reader = std::io::BufReader::new(fp);
                for line in buf_reader.lines() {
                    let line = line.unwrap();
                    let line = line.trim();
                    if line.len() != 0 {
                        let split: Vec<u32> = line.split(',').map(|x| x.parse().unwrap()).collect();
                        let &[from, to] = split.as_slice() else {panic!("invalid csv")};
                        send.send((from, to)).unwrap();
                    }
                }
            });
        }
        while let Ok((from, to)) = recv.recv() {
            self.node_max = max(self.node_max, max(to, from));
            while self.edge_to_lower.len() <= self.node_max as usize {
                self.edge_to_lower.push(vec![]);
                self.edge_from_lower.push(vec![]);
                self.edge_to_upper.push(vec![]);
                self.edge_from_upper.push(vec![]);
            }
            if to > from {
                self.edge_from_lower[to as usize].push(from);
                self.edge_to_upper[from as usize].push(to);
            } else if to < from {
                self.edge_to_lower[from as usize].push(to);
                self.edge_from_upper[to as usize].push(from);
            }
            self.edge_count += 1;
        }
    }

    fn prepare(&mut self) {
        for mlist in [
            &mut self.edge_from_lower,
            &mut self.edge_to_lower,
            &mut self.edge_to_upper,
            &mut self.edge_from_upper,
        ] {
            mlist.par_iter_mut().for_each(|list| {
                list.sort();
                list.shrink_to_fit();
            });
        }
    }

    fn enable_dump(&mut self, enable: bool) {
        self.dump = enable;
    }

    fn process_node(&self, node: u32) -> Stat {
        // println!("@node {}", node);
        let froms = &self.edge_from_lower[node as usize];
        // println!("@from {:?}", froms);
        let tos = &self.edge_to_lower[node as usize];
        // println!("@to {:?}", tos);
        let mut triangle_found = 0;
        let mut path2_count = 0;

        for &from in froms {
            // lower -> low -> node
            let candidate = &self.edge_from_lower[from as usize];
            // println!("@check {:?} -> {} -> {}", candidate, from, node);
            triangle_found += intersect_count_sorted(candidate, tos);
            path2_count += candidate.len();

            // low -> lower -> node
            path2_count += get_lower(&self.edge_from_upper[from as usize], node);

            // low -> node -> lower; TODO: optimize with merge
            path2_count += get_lower(tos, from);
        }
        for &to in tos {
            // node -> low -> lower
            let candidate = &self.edge_to_lower[to as usize];
            // println!("@check {:?} <- {} <- {}", candidate, to, node);
            triangle_found += intersect_count_sorted(candidate, froms);
            path2_count += candidate.len();

            // node -> lower -> low
            path2_count += get_lower(&self.edge_to_upper[to as usize], node);

            // lower -> node -> low; TODO: optimize with merge
            path2_count += get_lower(froms, to);
        }
        if self.dump {
            for &from in froms {
                self.edge_from_lower[from as usize]
                    .iter()
                    .filter(|&i| tos.contains(i)) // very slow!
                    .for_each(|i| println!("TRI  {} -> {} -> {}", i, from, node));
                // lower -> low -> node
                self.edge_from_lower[from as usize]
                    .iter()
                    .for_each(|i| println!("PATH {} -> {} -> {}", i, from, node));
                // low -> lower -> node
                self.edge_from_upper[from as usize]
                    .iter()
                    .filter(|&&i| i < node)
                    .for_each(|i| println!("PATH {} -> {} -> {}", i, from, node));
                // low -> node -> lower
                tos.iter()
                    .filter(|&&i| i < from)
                    .for_each(|i| println!("PATH {} -> {} -> {}", from, node, i));
            }
            for &to in tos {
                self.edge_to_lower[to as usize]
                    .iter()
                    .filter(|&i| froms.contains(i)) // very slow!
                    .for_each(|i| println!("TRI  {} -> {} -> {}", node, to, i));
                // node -> low -> lower
                self.edge_to_lower[to as usize]
                    .iter()
                    .for_each(|i| println!("PATH {} -> {} -> {}", node, to, i));
                // node -> lower -> low
                self.edge_to_upper[to as usize]
                    .iter()
                    .filter(|&&i| i < node)
                    .for_each(|i| println!("PATH {} -> {} -> {}", node, to, i));
                // lower -> node -> low
                froms
                    .iter()
                    .filter(|&&i| i < to)
                    .for_each(|i| println!("PATH {} -> {} -> {}", i, node, to));
            }
        }
        Stat {
            last_node: node,
            triangle_found,
            path2_count,
            edge_processed: froms.len() + tos.len(),
        }
    }
}

fn get_lower(vec: &Vec<u32>, high: u32) -> usize {
    match vec.binary_search(&high) {
        Ok(x) => x,
        Err(x) => x,
    }
}

fn intersect_count_sorted(a: &[u32], b: &[u32]) -> usize {
    let mut ptr_a = 0;
    let mut ptr_b = 0;
    let mut count = 0;
    while ptr_a < a.len() && ptr_b < b.len() {
        let va = a[ptr_a];
        let vb = b[ptr_b];
        if va == vb {
            count += 1;
            ptr_a += 1;
            ptr_b += 1;
        } else if va < vb {
            ptr_a += 1;
        } else {
            ptr_b += 1;
        }
    }
    return count;
}

fn dump_stats_head(target: &mut impl Write, c: &Context) {
    writeln!(target, "# node_max: {}", c.node_max).unwrap();
    writeln!(target, "# edge_count: {}", c.edge_count).unwrap();
    writeln!(
        target,
        "last_node,edge_processed,path2_count,triangle_found"
    )
    .unwrap();
}

impl Stat {
    fn combine(&mut self, prev: &Self) {
        self.last_node = max(self.last_node, prev.last_node);
        self.edge_processed += prev.edge_processed;
        self.path2_count += prev.path2_count;
        self.triangle_found += prev.triangle_found;
    }
    fn dump(&self, target: &mut impl Write) {
        writeln!(
            target,
            "{},{},{},{}",
            self.last_node, self.edge_processed, self.path2_count, self.triangle_found
        )
        .unwrap();
    }
}

#[derive(FromArgs)]
#[argh(help_triggers("-h", "--help"))]
/// Count triangle
pub struct Args {
    /// dump path2 and triangle to stdout
    #[argh(switch, short = 'd')]
    dump: bool,

    /// input edges.csv
    #[argh(positional)]
    input: PathBuf,

    /// output path
    #[argh(option, short = 'o', default = "\"log.csv\".into()")]
    output: PathBuf,
}

fn main() {
    let args: Args = argh::from_env();

    if !args.input.exists() {
        eprintln!("input file not exists");
        std::process::exit(-1);
    }

    let mut start;
    let mut c = Context::new();
    let log = std::fs::File::create(args.output).unwrap();
    let mut log = std::io::BufWriter::new(log);
    eprintln!("Parsing csv");
    start = Instant::now();
    {
        let fp = std::fs::File::open(args.input).expect("Can't open input file");
        c.parse_edge(fp);
    }
    dump_stats_head(&mut log, &c);
    eprintln!("Parsing csv OK [{:.3} sec]", start.elapsed().as_secs_f32());
    eprintln!("Preparing data");
    start = Instant::now();
    c.prepare();
    eprintln!(
        "Preparing data OK [{:.3} sec]",
        start.elapsed().as_secs_f32()
    );
    eprintln!("Processing");
    start = Instant::now();
    let stats: Vec<_> = if args.dump {
        c.enable_dump(true);
        // Don't parallel when dumping
        (2..=c.node_max).map(|node| c.process_node(node)).collect()
    } else {
        (2..=c.node_max)
            .into_par_iter()
            .map(|node| c.process_node(node))
            .collect()
    };
    eprintln!("Processing OK [{:.3} sec]", start.elapsed().as_secs_f32());
    eprintln!("Dumping");
    start = Instant::now();
    stats
        .iter()
        .scan(Stat::default(), |ctx, next| {
            ctx.combine(next);
            Some(ctx.clone())
        })
        .for_each(|stat| {
            stat.dump(&mut log);
        });
    eprintln!("Dumping OK [{:.3} sec]", start.elapsed().as_secs_f32());
}

#[cfg(test)]
mod tests {
    use std::path::PathBuf;

    use super::*;

    fn run(name: &str, contents: &[u8]) -> Stat {
        let temp_file: PathBuf = format!("test_{}.csv", name).into();
        std::fs::write(&temp_file, contents).unwrap();
        let mut c = Context::new();
        c.enable_dump(true);
        c.parse_edge(std::fs::File::open(&temp_file).unwrap());
        c.prepare();
        let stat = (2..=c.node_max)
            .map(|node| {
                let stat = c.process_node(node);
                // let mut stdout = std::io::stdout();
                // stat.dump(&mut stdout);
                stat
            })
            .fold(Stat::default(), |mut sum, next| {
                sum.combine(&next);
                sum
            });
        return stat;
    }

    #[test]
    fn one_tri() {
        let contents = br#"
        1,2
        2,3
        3,1
        "#;
        assert_eq!(
            run("one_tri", contents),
            Stat {
                last_node: 3,
                edge_processed: 3,
                path2_count: 3,
                triangle_found: 1
            }
        );
    }

    #[test]
    fn path2() {
        let contents = br#"
        1,2
        2,3
        3,4
        3,1
        1,3
        "#;
        // 1->2->3
        // 2->3->4
        // 2->3->1
        // 3->1->2
        // 1->3->4
        assert_eq!(
            run("path2", contents),
            Stat {
                last_node: 4,
                edge_processed: 5,
                path2_count: 5,
                triangle_found: 1
            }
        );
    }
}
