#!/usr/bin/env python3
import random
import os
from pathlib import Path

OUT_PATH = Path('testcase')
NODE_MAX = 20
EDGES_COUNT = 300

OUT_PATH.mkdir(parents=True, exist_ok=True)
EDGES = OUT_PATH / 'edges.csv'
NODES = OUT_PATH / 'nodes.csv'
STATS = OUT_PATH / 'stats.csv'
DUMP = OUT_PATH / 'dump.txt'

dup_chuck = set()
with open(EDGES, 'w') as fp:
	for i in range(EDGES_COUNT):
		while True:
			fro, to = random.randint(1,NODE_MAX), random.randint(1,NODE_MAX)
			if (fro, to) not in dup_chuck:
				dup_chuck.add((fro, to))
				break
		print(f'{fro},{to}', file=fp)

with open(NODES, 'w') as fp:
	for i in range(1,1+NODE_MAX):
		print(i, file=fp)

os.system(f'cargo run -- {EDGES} -o {STATS} -d > {DUMP}')
