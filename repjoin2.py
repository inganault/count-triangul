
from pathlib import Path
import time
import multiprocessing
import sys

# Run with PyPy for better performance

INPUT_PATH = Path(sys.argv[1]) if len(sys.argv) > 1 else Path('testcase')
EDGES = INPUT_PATH / 'edges.csv'
NODES = INPUT_PATH / 'nodes.csv'

SHOW_PROGRESS = True
WORKER_COUNT = 4
CHUNK_COUNT = 1000
USE_HASH = False

def setup(master=False, log=False):
    # Setup, 1 time per worker
    global edge_to_lower, edge_from_lower, node_max

    # Get node_max from nodes.csv
    with open(NODES) as fp:
        last_line = None
        for line in fp:
            if len(line) > 1:
                last_line = line
        node_max = int(last_line)
    
    if not master:
        # Parse edge to array in memory
        if log:
            print(f'Reading edges')
        start = time.monotonic()
        if USE_HASH:
            edge_from_lower = [set() for _ in range(node_max+1)]
            edge_to_lower = [set() for _ in range(node_max+1)]
            with open(EDGES) as fp:
                for lin in fp:
                    fro,to = map(int,lin.split(','))
                    if fro == to:
                        continue
                    if to > fro:
                        edge_from_lower[to].add(fro)
                    else:
                        edge_to_lower[fro].add(to)
        else:
            edge_from_lower = [[] for _ in range(node_max+1)]
            edge_to_lower = [[] for _ in range(node_max+1)]
            with open(EDGES) as fp:
                for lin in fp:
                    fro,to = map(int,lin.split(','))
                    if fro == to:
                        continue
                    if to > fro:
                        edge_from_lower[to].append(fro)
                    else:
                        edge_to_lower[fro].append(to)

            for mlist in (edge_from_lower, edge_to_lower):
                for list in mlist:
                    list.sort()

        if log:
            print(f'Reading edges [{time.monotonic()-start:.2f} sec]')

def intersect_count_sorted_list(a, b) -> int:
    ptr_a = 0
    ptr_b = 0
    count = 0
    while ptr_a < len(a) and ptr_b < len(b):
        va = a[ptr_a]
        vb = b[ptr_b]
        if va == vb:
            count += 1
            ptr_a += 1
            ptr_b += 1
        elif va < vb:
            ptr_a += 1
        else:
            ptr_b += 1
    return count

def intersect_count_sorted_set(a, b) -> int:
    return len(a & b)

if USE_HASH:
    intersect_count_sorted = intersect_count_sorted_set
else:
    intersect_count_sorted = intersect_count_sorted_list


def mapper(set_n):
    count = 0
    for node in set_n:
        froms = edge_from_lower[node]
        tos = edge_to_lower[node]

        for fro in froms:
            # lower -> low -> node
            candidate = edge_from_lower[fro]
            count += intersect_count_sorted(candidate, tos)
        for to in tos:
            # node -> low -> lower
            candidate = edge_to_lower[to]
            count += intersect_count_sorted(candidate, froms)
    if SHOW_PROGRESS:
        progress = ((max(set_n)-1) / node_max) * 100
        print(f'Progress: {progress:5.2f}%', end='\r')
    return count

if __name__ == '__main__':
    setup(master=True)
    chunk_size = max(1, (node_max+CHUNK_COUNT-1)//CHUNK_COUNT)
    chunks = (list(range(node, min(node_max+1, node+chunk_size))) for node in range(1, node_max+1, chunk_size))
    start = time.monotonic()
    if SHOW_PROGRESS:
        print('Processing')
    with multiprocessing.Pool(WORKER_COUNT) as p:
        count = sum(p.imap(mapper, chunks))
    if SHOW_PROGRESS:
        print(f'Finished [{time.monotonic()-start:.2f} sec]')
        print('\n------')
    print(f'Count = {count}')
else:
    setup(master=False)
